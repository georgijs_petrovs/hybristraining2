/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.training.storefront.controllers.cms;

import de.hybris.platform.acceleratorfacades.productcarousel.ProductCarouselFacade;
import de.hybris.platform.cms2lib.model.components.ProductCarouselComponentModel;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.search.ProductSearchFacade;
import de.hybris.platform.commercefacades.search.data.SearchQueryData;
import de.hybris.platform.commercefacades.search.data.SearchStateData;
import de.hybris.platform.commercefacades.storesession.StoreSessionFacade;
import de.hybris.training.core.model.cms.components.SimplePDFComponentModel;
import de.hybris.training.storefront.controllers.ControllerConstants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Controller for CMS ProductReferencesComponent.
 */
@Controller("SimplePDFComponentController")
@RequestMapping(value = ControllerConstants.Actions.Cms.SimplePDFComponent)
public class SimplePDFComponentController extends AbstractAcceleratorCMSComponentController<SimplePDFComponentModel>
{
    @Resource(name = "storeSessionFacade")
    private StoreSessionFacade storeSessionFacade;

    @Resource(name = "customerFacade")
    private CustomerFacade customerFacade;

    @Override
    protected void fillModel(final HttpServletRequest request, final Model model, final SimplePDFComponentModel component)
    {
        model.addAttribute("currency", storeSessionFacade.getCurrentCurrency().getIsocode());
        model.addAttribute("info", "Add attribute to the page.");
        model.addAttribute("pdf", component.getPdf());
        model.addAttribute("media", component.getMedia());
        if(customerFacade.getCurrentCustomer().getFirstName().equals("Georgijs")){
            model.addAttribute("name", "JA RABOTAJU");
        }
    }

}