package de.hybris.training.facades.forms;

public class UpdateProfileForm {

    private String titleCode;
    private String firstName;
    private String lastName;
    private String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getTitleCode() {
        return titleCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setTitleCode(String titleCode) {
        this.titleCode = titleCode;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
