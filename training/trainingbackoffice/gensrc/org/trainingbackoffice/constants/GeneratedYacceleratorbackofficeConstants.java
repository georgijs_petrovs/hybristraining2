/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jan 8, 2020, 2:51:12 PM                     ---
 * ----------------------------------------------------------------
 */
package org.trainingbackoffice.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedYacceleratorbackofficeConstants
{
	public static final String EXTENSIONNAME = "trainingbackoffice";
	
	protected GeneratedYacceleratorbackofficeConstants()
	{
		// private constructor
	}
	
	
}
