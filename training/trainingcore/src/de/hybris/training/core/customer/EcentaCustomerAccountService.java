package de.hybris.training.core.customer;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.core.model.user.CustomerModel;

public interface EcentaCustomerAccountService extends CustomerAccountService {

    public void updateProfile(final CustomerModel customerModel, final String titleCode, final String name, final String login, final String phoneNumber) throws DuplicateUidException;
}
